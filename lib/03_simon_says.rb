def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, times_to_repeat = 2)
  repeated_string_arr = []
  times_to_repeat.times { repeated_string_arr << string }
  repeated_string_arr.join(" ")
end

def start_of_word(word, number_of_letters)
  letters_str = ""

  i = 0
  while i < number_of_letters
    letters_str << word[i]
    i += 1
  end

  letters_str
end

def first_word(string)
  words = string.split(" ")
  words[0]
end

def titleize(string)
  little_words = ["a", "an", "the", "and", "over", "at"]
  words = string.split(" ")

  titleized_words = words.each_with_index.map do |word, i|
    if !(little_words.include?(word)) || i == 0
      word.capitalize
    else
      word
    end
  end

  titleized_words.join(" ")
end
