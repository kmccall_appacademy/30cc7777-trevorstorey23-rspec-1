def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  numbers.inject(0) { |accum, next_num| accum + next_num }
end

def multiply(numbers)
  numbers.inject(1) { |product, next_num| product * next_num }
end

def power(base, exponent)
  result = base
  exponent-1.times { result *= base }
  result
end

def factorial(num)
  return 1 if num == 0
  multiply((1..num))
end
