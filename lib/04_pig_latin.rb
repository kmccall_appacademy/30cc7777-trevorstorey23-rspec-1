#require 'byebug'

def vowel?(letter)
  vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
  vowels.include?(letter)
end

def starts_with_vowel?(word)
  vowel?(word[0])
end

def vowel_first_pig_word(word)
  word + "ay"
end

def consonant_first_pig_word(word)
  i = 0
  while i < word.length
    letter = word[i]

    if vowel?(letter)
      i += 1 if letter == "u" && word[i-1] == "q"

      up_to_first_vowel = word[0...i]
      from_first_vowel = word[i..-1]
      pigged_word = from_first_vowel + up_to_first_vowel + "ay"
      break
    end

    i += 1
  end

  pigged_word
end

def punctuation?(char)
  not_punctuation_marks = "abcdefghijklmnopqrstuvwxyz0123456789".split("")
  !(not_punctuation_marks.include?(char.downcase))
end

def removed_punctuation(word)
  no_punc_word = ""
  word.each_char { |char| no_punc_word << char if !punctuation?(char)  }
  no_punc_word
end

def map_original_index_to_punctuation(word)
  index_punctuation_pairs = {}
  i = 0
  while i < word.length
    char = word[i]
    index_punctuation_pairs[i.to_s] = char if punctuation?(char)
    i += 1
  end
  index_punctuation_pairs
end

def repunctuate(no_punc_word, mapped_punctuation)
  repunctuated_word = no_punc_word
  mapped_punctuation.each do |index, char|
    #  add 2 to index insert position if the punctuation was not at the beginning of the word
    #   because "ay" is always added to the end of the original word to pig it.
    insert_pos = index == "0" ? index.to_i : index.to_i + 2
    repunctuated_word.insert(insert_pos, char)
  end
  repunctuated_word
end

def pig_word(word)
  mapped_punc = map_original_index_to_punctuation(word)
  no_punc_word = removed_punctuation(word)

  if starts_with_vowel?(no_punc_word)
    no_punc_pigged_word = vowel_first_pig_word(no_punc_word)
  else
    no_punc_pigged_word = consonant_first_pig_word(no_punc_word)
  end
  
  no_punc_pigged_word.capitalize! if no_punc_word[0].upcase == no_punc_word[0]
  repunctuate(no_punc_pigged_word, mapped_punc)
end

def translate(sentence)
    words = sentence.split(" ")
    pig_words = words.map { |word| pig_word(word) }
    pig_words.join(" ")
end
